# -*- coding: utf-8 -*-
"""
Created on Tue May 21 17:28:41 2019

@author: abrini
"""
import os
from keras.models import load_model
import pandas as pd
import numpy as np
import netCDF4 as nc
import numpy.ma as ma
from sklearn.externals import joblib
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping
from models.mlp_model import mlp_1000_500
import warnings
warnings.filterwarnings("ignore")

def loop_lon_lat(lat_len,lon_len):
    for i in range(lat_len):
        for j in range(lon_len):
            yield i, j



def read_table(path,variable,year,month):
    var_file_path = path + "/" + str(year) + "/" + str(month) + "/" + str(variable) +"_" + str(year) +"{:02d}".format(month)+".nc"
    var_file = nc.Dataset(var_file_path)
    var = var_file.variables[str(variable)][:].data
    return {variable:var} 


def mon_sin(x):
    return np.sin(2*np.pi*x/12)

def mon_cos(x):
    return np.cos(2*np.pi*x/12)

def recreate_regional(models, gendata_path,scalers,feature_names=None,start=2012,end=2016,shape=[1,13],lat_sin=True,verbose=False):
  
    path = "./data/INPUT_AI/"
    lon_lat_path = "./data/INPUT_AI/1999/10/chl_199910.nc"
    ncfile = nc.Dataset(lon_lat_path)
    lons = ncfile.variables['lon'][:].data
    lats = ncfile.variables['lat'][:].data
    ncfile.close()
    chls = []
    if feature_names == None:
        feature_names = ["sla","sst","uera","vera","u","v","sw"]
    try:
        os.mkdir("./"+gendata_path+"{:d}-{:d}".format(start,end))
        print("Directory " , "./"+gendata_path+"{:d}-{:d}".format(start,end) ,  " Created ") 
    except FileExistsError:
        print("Directory " , "./"+gendata_path+"{:d}-{:d}".format(start,end) ,  " already exists")
            
    for year in range(start,end):
        months = [int(item) for item in os.listdir(path+"/"+str(year)) ]
        months.sort()
        for month in months:
            data = [read_table(path,item,year,month) for item in feature_names ]
            chl = np.full([178,358],np.NaN)
            for i,j in loop_lon_lat(178,358):
                features = [data[index][feat_name][0,i,j] for index, feat_name in enumerate(feature_names)]
                if -9999.0 in features:
                    continue
                
                ###MODEL SELECTION###

                    
                lat = lats[i]
                lon = lons[i]
                flag = False
                ### NA = 0, SA = 1, AU 2, NP = 3, SP = 4, IN = 5
                if 46 <= lat:
                    if 15 < lon and lon <= 250:
                        seg = "NP"
                    else:
                        seg = "NA"
                if -24 <= lat and lat <=24:
                    if 15 < lon and lon <=110:
                        seg = "IN"
                    if 110 < lon and lon <=275:
                        seg = "SP"
                    else:
                        seg = "SA"
                if lat <=-46:
                    seg = "AU"
                
                if 25 <= lat and lat <=45:
                    flag = True
                    if 15 < lon and lon <= 250:
                        seg1 = "NP"
                        seg2 = "SP"
                    else:
                        seg1 = "NA"
                        seg2 = "SA"
                if -45 <= lat and lat <= -25:
                    flag = True
                    if 15 < lon and lon <=110:
                        seg1 = "IN"
                        seg2 = "AU"
                    if 110 < lon and lon <=275:
                        seg1 = "SP"
                        seg2 = "AU"
                    else:
                        seg1 = "SA"
                        seg2 = "AU"
                                        
                                   
                #### PREPROCESSING####
                if lat_sin == True:
                    lat = np.sin(lat)
                else:
                    lat = lat
                    
                lon1 = np.sin(lons[j])
                lon2 = np.cos(lons[j])
                mon_1 = mon_sin(int(month))
                mon_2 = mon_cos(int(month))
                #lat, year, sla , sst, uera, vera , u, v, sw,lon1,lon2,mon_1,mon_2
                features.insert(0,lat)
                features.insert(1,year)
                features.insert(9,lon1)
                features.insert(10,lon2)
                features.insert(11,mon_1)
                features.insert(12,mon_2)
                ###SCALING###
                ##PREDICTION##
                if flag:
                    features1 = (np.array(features) - scalers[seg1].mean_[1::])/scalers[seg1].scale_[1::]
                    
                    features2 = (np.array(features) - scalers[seg2].mean_[1::])/scalers[seg2].scale_[1::]
                    pred1 = ma.exp(models[seg1].predict((np.reshape(features1,shape)-scalers[seg1].mean_[0])/scalers[seg1].scale_[0]))
                    pred2 = ma.exp(models[seg2].predict((np.reshape(features2,shape)-scalers[seg2].mean_[0])/scalers[seg2].scale_[0]))
                    chl[i,j] = ma.mean([pred1,pred2])
                else:
                    features = (np.array(features) - scalers[seg].mean_[1::])/scalers[seg].scale_[1::]
                    chl[i,j] = ma.exp(models[seg].predict((np.reshape(features,shape)-scalers[seg].mean_[0])/scalers[seg].scale_[0]))
                
            if verbose == True:
                print(str(year)+" "+str(month))
            chls.append(chl)

            np.savetxt(gendata_path+"{:d}-{:d}".format(start,end)+"/Chl_"+str(year)+"{:02d}".format(month)+".dat",chl)
    return np.stack(chls,axis=0)



def prep_scale(data):
    np.random.seed(0)
    data.loc[:,"lat"] = data["lat"].apply(np.sin).to_numpy()
    data["lon_1"] = data["lon"].apply(np.sin).to_numpy()
    data["lon_2"] = data["lon"].apply(np.cos).to_numpy()
    data["mon_1"] = data["mon"].apply(mon_sin).to_numpy()
    data["mon_2"] = data["mon"].apply(mon_cos).to_numpy()
    data["hl_"] = data["hl_"].apply(np.log).to_numpy()
    data = data.drop(columns=["lon","mon"])

    return data

def train_slice(slice_path, scaler,N):
    SL_name = slice_path.split("_")[-1].split(".")[0]
    print("Training "+SL_name+" .... ")
    data = pd.read_csv(slice_path)
    print("Scaling "+SL_name+" of shape"+str(data.shape)+" .... ")
    try:
        data = data.drop(columns=["Unnamed: 0"])
    except:
        pass
    data = prep_scale(data)
    data = scaler.transform(data)
    
    X = data[:,1::] 
    Y = data[:,0]
    X_train, X_val, y_train, y_val = train_test_split(X, Y, test_size=0.2, random_state=0)
    tensorboard = TensorBoard(log_dir="tensorboardlogs/MLP_REG"+SL_name+N)
    model_file_name = './models/MLP_regional'+SL_name+N+'.h5'
    mc = ModelCheckpoint(model_file_name, monitor='val_loss')
    es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=80)
    if os.path.isfile(model_file_name):
        print("Loading Model File ....")
        m = load_model(model_file_name)
    else:
        print("Creating New Model")
        m = mlp_1000_500()

    print(m.summary())
    m.fit(X_train, y_train,verbose=1,batch_size=256,epochs=600,validation_data=[X_val, y_val], callbacks=[tensorboard, mc,es] )
    return m



if __name__=="__main__":
    
    print("Reading Data")

    data = pd.read_csv("data/occci_data.csv")
    data = data[data["hl_"] != -9999.0]
    data = data[data["sla_"] != -9999.0]
    data = data[data["u_"] != -9999.0]
    data = data[data["v_"] != -9999.0]
    data = data[data["uera_"] != -9999.0]
    data = data[data["vera_"] != -9999.0]
    
       
    print("Splitting data")

    data_NA = data[ (data["lat"]>= 35) & (data["lon"]>=250)]
    data_NA1 = data[ (data["lat"]>= 35) & (data["lon"]<=15)]
    data_NA = pd.concat([data_NA,data_NA1]).drop_duplicates().reset_index(drop=True)
    data_NA.to_csv('data/reg/occci_NA.csv')
    
    data_SA = data[(data["lat"]<=35) & (data["lat"]>=-35)  & (data["lon"]<=15)]
    data_SA1 =  data[(data["lat"]<=35) & (data["lat"]>=-35)  & (data["lon"]>=275)]
    data_SA = pd.concat([data_SA,data_SA1]).drop_duplicates().reset_index(drop=True)
    data_SA.to_csv('data/reg/occci_SA.csv',index=False)
    
    data_AU = data[data["lat"]<=-35]
    data_AU.to_csv('data/reg/occci_AU.csv',index=False)
    
    data_SP = data[(data["lat"]<=35) & (data["lat"]>=-35) & (data["lon"]<=275) & (data["lon"]>=110)]
    data_SP.to_csv('data/reg/occci_SP.csv',index=False)
    
    data_NP = data[(data["lat"]>=35) & (data["lon"]<=250)  & (data["lon"]>=15)]
    data_NP.to_csv('data/reg/occci_NP.csv',index=False)
    
    data_IN = data[(data["lat"]<=35) & (data["lat"]>=-35)  & (data["lon"]<=110)  & (data["lon"]>=15)] 
    data_IN.to_csv('data/reg/occci_IN.csv',index=False)
    
    print("Preparing Scalers1")
    scalers1 = {"NA":StandardScaler().fit(prep_scale(data_NA)),"SA":StandardScaler().fit(prep_scale(data_SA))
    ,"NP":StandardScaler().fit(prep_scale(data_NP)),"SP":StandardScaler().fit(prep_scale(data_SP))
    ,"IN":StandardScaler().fit(prep_scale(data_IN)),"AU":StandardScaler().fit(prep_scale(data_AU))}
 
    sc = StandardScaler().fit(prep_scale(data))
    print("Preparing Scalers2")
    scalers2 = {"NA":sc,"SA":sc,"NP":sc,"SP":sc,"IN":sc,"AU":sc}

    paths = ['data/reg/occci_AU.csv',
     'data/reg/occci_NP.csv',
     'data/reg/occci_NA.csv',
     'data/reg/occci_SA.csv',    

     'data/reg/occci_SP.csv',
     'data/reg/occci_IN.csv']
    
    #paths = ["./data/occci_XLL.csv"]
#    models1 = {"NA":train_slice(paths[2],scalers1["NA"],"111"),"NP":train_slice(paths[1],scalers1["NP"],"111"),
#              "SA":train_slice(paths[3],scalers1["SA"],"111"), "SP":train_slice(paths[4],scalers1["SP"],"111"),
#              "AU":train_slice(paths[0],scalers1["AU"],"111"),"IN":train_slice(paths[5],scalers1["IN"],"111")
#              }

    
#    models = {"NA":load_model("models/mlp_1000_500_NA.h5"),"SA":load_model("models/mlp_1000_500_SA.h5")
#    ,"NP":load_model("models/mlp_1000_500_NP.h5"),"SP":load_model("models/mlp_1000_500_SP.h5")
 #   ,"IN":load_model("models/mlp_1000_500_IN.h5"),"AU":load_model("models/mlp_1000_500_AU.h5")}
    
    
    
#    rec_data = recreate_regional(models1,"generated data/mlp_regional_ref111",scalers1,start=1998,end=2016,verbose=True)
    

    models2 = {"NA":train_slice(paths[2],scalers2["NA"],"222"),"NP":train_slice(paths[1],scalers2["NP"],"222"),
              "SA":train_slice(paths[3],scalers2["SA"],"222"), "SP":train_slice(paths[4],scalers2["SP"],"222"),
              "AU":train_slice(paths[0],scalers2["AU"],"222"),"IN":train_slice(paths[5],scalers2["IN"],"222")
              }
    
    
    rec_data2 = recreate_regional(models2,"generated data/mlp_regional_ref222",scalers2,start=1998,end=2016,verbose=True)
