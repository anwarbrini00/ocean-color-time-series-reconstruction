# -*- coding: utf-8 -*-
"""
Created on Mon Jun 24 14:46:34 2019

@author: anwar
"""

from torch import nn
import torch

def swish(input_vector):
    """ 
    Parameters
    ----------
    x : Torch Tensor, Numpy array

    Returns
    -------
    TYPE Torch Tensor
        swish function of the input.

    """
    return input_vector * torch.sigmoid(input_vector)

class Cnn0(nn.Module):
    """
    First CNN Class with Encode-Decode via ConvTranspose layers
    """
    def __init__(self):
        super(Cnn0, self).__init__()
        self.activation_layer = swish
        self.conv1_1 = nn.Conv2d(10, 32, 3, padding=1)
        self.conv1_2 = nn.Conv2d(32, 32, 3, padding=1)
        self.pool1 = nn.MaxPool2d(2)
        self.conv2_1 = nn.Conv2d(32, 64, 2, padding=1)
        self.conv2_2 = nn.Conv2d(64, 64, 2, padding=1)
        self.dropout1 = nn.Dropout(0.85)
        self.pool2 = nn.MaxPool2d(2)
        self.conv3_1 = nn.Conv2d(64, 128, 2, padding=1)
        self.conv3_2 = nn.Conv2d(128, 128, 2, padding=1)
        self.dropout2 = nn.Dropout(0.85)
        self.pool3 = nn.MaxPool2d(2)
        self.trans4_1 = nn.ConvTranspose2d(128, 64, 10, dilation=(1, 2),
                                           stride=(2, 2), padding=1)
        self.trans5_1 = nn.ConvTranspose2d(64, 32, 20, dilation=(1, 3),
                                           stride=(2, 2), padding=1, output_padding=(0, 1))
        self.trans6_1 = nn.ConvTranspose2d(32, 1, 32, dilation=(2, 3),
                                           stride=(1, 1), padding=2)

    def forward(self, x):
        layer_out = x
        layer_out = self.conv1_1(layer_out)
        layer_out = self.activation_layer(layer_out)
        layer_out = self.activation_layer(self.conv1_2(layer_out))
        layer_out = self.pool1(layer_out)
        layer_out = self.activation_layer(self.conv2_1(layer_out))
        layer_out = self.dropout1(self.activation_layer(self.conv2_2(layer_out)))
        layer_out = self.pool2(layer_out)
        layer_out = self.activation_layer(self.conv3_1(layer_out))
        layer_out = self.dropout2(self.activation_layer(self.conv3_2(layer_out)))
        layer_out = self.pool3(layer_out)
        layer_out = self.activation_layer(self.trans4_1(layer_out))
        layer_out = self.activation_layer(self.trans5_1(layer_out))
        layer_out = self.trans6_1(layer_out)
        return layer_out
