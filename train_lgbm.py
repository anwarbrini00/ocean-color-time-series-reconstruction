# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 15:03:12 2019

@author: abrini
"""

import pandas as pd
import lightgbm as lgb
import json
from scipy.stats import norm
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error
import preprocess
import matplotlib.pyplot as plt
import numpy as np



def data_split(data,t_size=0.1):
    s = StandardScaler()
    x_cols = list(data.columns)
    x_cols.pop(0)
    x = s.fit_transform(data[x_cols])
    y = data["hl_"]
    from sklearn.model_selection import train_test_split
    X_train, X_val, y_train, y_val = train_test_split(x, y, test_size=t_size, random_state=0)
    
    return (X_train, X_val, y_train, y_val, x_cols)

def model_train(X_train, X_val, y_train, y_val, x_cols,model_name=None):
    params = {
    'boosting_type': 'gbdt',
    'objective': 'regression',
    'metric': 'l2',
    'learning_rate': 0.010,
    'num_leaves': 256,  
    'max_depth': 130,   
    'subsample_for_bin': 200000,    
   # 'nthread': 8,
    'verbose': 20,
#     'scale_pos_weight'
    }
    cols = x_cols
    
    dtrain = lgb.Dataset(X_train[cols].values, label=y_train.values)
    dvalid = lgb.Dataset(X_val[cols].values, label=y_val.values)
    
    evals_result = {}  # to record eval results for plotting

    lgb_model = lgb.train(params, 
                 dtrain, 
                 valid_sets=[dtrain, dvalid], 
                 valid_names=['train','valid'], 
                 evals_result=evals_result,
                 num_boost_round=15000,
                 feature_name=cols,
                 early_stopping_rounds=50,
                 verbose_eval=5)
    
    lgb_model = lgb.train(params,
                 dtrain, 
                 valid_sets=[dtrain, dvalid], 
                 valid_names=['train','valid'], 
                 evals_result=evals_result,
                 num_boost_round=15000,
                 feature_name=cols,
                 early_stopping_rounds=50,
                 verbose_eval=5,
                 init_model=model_name)

    return lgb_model,   evals_result



if __name__ == "__main__":
    data = pd.read_csv("./data/u_data.csv")
    data = preprocess.to_2DNNformat(data)
    X_train, X_val, y_train, y_val, x_cols = data_split(data)
    data = None # to free memory
    lgb_model,   evals_result = model_train(X_train, X_val, y_train, y_val, x_cols)
    print('Saving model...')
    lgb_model.save_model('lgb_model.txt')
    ax = lgb.plot_metric(evals_result, metric='l2')
    plt.savefig('lgb_metrics')






