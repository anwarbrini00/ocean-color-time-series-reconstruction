# -*- coding: utf-8 -*-
"""
Created on Fri Apr  5 10:26:39 2019

@author: anwar
"""

import glob
import numpy as np
import pandas as pd
import netCDF4 as nc
import os
from string import digits

BASE = "./data/"
slash = "/"
remove_digits = str.maketrans('','', digits)

def var_name(string):
    return string.split("_")[0]


def flatten_cords(single_mat):
    XX,YY = np.meshgrid(np.arange(single_mat.shape[1]),np.arange(single_mat.shape[0]))
    flattened = np.vstack((single_mat.ravel(),XX.ravel(),YY.ravel())).T
    return flattened


def all_columns(path):
    L = []
    for f_index, file in enumerate(os.listdir(path)):
        L.append(file.strip(".nc").translate(remove_digits))
    return L   
        
def lon_lat_cols(path):
    s_data = nc.Dataset(path ,"r",format="NETCDF4")
    a_data = flatten_cords(s_data["chl"][:].data[0])
    x = a_data[:,1]
    y = a_data[:,2]
    return np.column_stack((x,y))


def filter_var(item):
    return "__" not in item
       
def rm_number(filename):
    return  filename.strip(".nc").translate(remove_digits)


def read_curr_vars(BASE):
    varz = []
    for year in os.listdir(BASE):
        months = os.listdir(BASE + slash + year)
        months.sort()
        for month in months:
            files = {}
            path = BASE + slash + year + slash + month 
            for file in os.listdir(path):
                if "__" not in (rm_number(file)):
                    file_path = path + slash + file
                    f_data = nc.Dataset(file_path,"r",format="NETCDF4")
                    data = f_data.variables[var_name(file)][:].data[0]
                    f_data.close()
                    var_n = file.strip(".nc").translate(remove_digits)
                    files.update({var_n:data})
            varz.append(files)
    return varz


def nc_to_df(path,lon_lat,cols, lon=358,lat=178,num_months=12,num_files=70):
    YEARS = os.listdir(path)
    global_df = pd.DataFrame()
    for y_index, year in enumerate(YEARS):
        for m_index, month in enumerate(os.listdir(path + slash + year)):
            y = np.array([year]*len(lon_lat))
            m = np.array([month]*len(lon_lat))
            local_df = pd.DataFrame(np.column_stack((y,m,lon_lat)).astype(float),columns=["year","mon","lon","lat"]) 
            for f_index, file in enumerate(os.listdir(path + slash + year + slash + month )):
                file_path = path + slash + year + slash + month + slash + file
              #  print("Reading: "+file_path)
                s_data = nc.Dataset(file_path ,"r",format="NETCDF4")
                s_data = s_data.variables[var_name(file)][:].data[0]
                a_data = flatten_cords(s_data)
                var_ = file.strip(".nc").translate(remove_digits)
                file_pd = pd.DataFrame({var_:a_data[:,0],"lon":a_data[:,1],"lat":a_data[:,2]})
                local_df = pd.merge(local_df,file_pd, on=["lon","lat"] )
                print("Loaded: "+file)
            global_df = pd.concat([global_df, local_df],ignore_index=True)
    return global_df


def copy_chl_files(path_from,path_to):
    from shutil import copyfile
    years = os.listdir(path_to)
    for year in years:
        months  = os.listdir(path_to+"/"+year)
        for month in months:
            src = path_from + "/" + "chl_"+ str(year) +"{:02d}".format(int(month)) + ".nc"
            dst = path_to + "/"+ year +"/"+ month + "/" + "chl_"+ str(year) +"{:02d}".format(int(month)) + ".nc"
            copyfile(src, dst)
            

def create_lats_lons(path):  
    dataset = nc.Dataset(path)
    lats = dataset.variables['lat'][:]
    lons = dataset.variables['lon'][:]
    lats.dump("lats.npy")
    lons.dump("lons.npy")
    

if __name__ == "__main__":
    BASE = "data/GC_data"
    cols = all_columns(BASE + slash + "2005" + slash + "5" )
    [cols.append(x) for x in ["lon","lat","month","year"]]
    L = lon_lat_cols(BASE + slash + "2005" + slash + "5" + "/chl_200505.nc"  )
    data = nc_to_df(BASE,cols=cols,lon_lat=L)
    data = data[["hl_","lon","lat","mon","year","sla_","sst_","u_","uera_","v_","vera_","sw_"]]
    print(data.shape)
    data.to_csv("gc_flattened_data.csv")
    