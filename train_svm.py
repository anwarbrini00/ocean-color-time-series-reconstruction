import pandas as pd
import numpy as np
from sklearn import svm
from preprocess_flat import preprocess_flat_data
from joblib import dump, load



def split_scale_data(data):
    from sklearn.model_selection import train_test_split
    from sklearn.preprocessing import StandardScaler
    l = list(data.columns)
    l.pop(0)
    Y = np.log(data["hl_"])
    X = data[l]
    s = StandardScaler()
    scaled_X = s.fit_transform(X.values)
    X_train, X_val, y_train, y_val = train_test_split(scaled_X, Y, test_size=0.91, random_state=10)
    return X_train, X_val, y_train, y_val, s

if __name__ == "__main__":
    data = pd.read_csv(("./data/gc_flattened_data.csv"))
    data = preprocess_flat_data(data)
    data_ = data
    X_train, X_val, y_train, y_val, s = split_scale_data(data_)
    clf = svm.SVR(kernel='rbf',verbose=True,max_iter=-1,cache_size=1000,C=1,epsilon=.1,gamma=0.1)
    clf.fit(X_train, y_train)
    dump(clf, 'svr_gc.joblib')