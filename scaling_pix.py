# -*- coding: utf-8 -*-
import glob
import netCDF4 as nc
import numpy as np
import numpy.ma as ma
from sklearn.preprocessing import StandardScaler
import pandas as pd
from sklearn.externals import joblib 

def sort_over_folders(path,variable):
    files = glob.glob(path+"/*/*/"+variable+"_[0-9]*[0-9].nc")
    n_files = []
    r_files = []
    for file in files:
        s = file.split("/")
        s[3] = "{:02d}".format(int(s[3]))
        s = '/'.join(s)
        n_files.append(s)
    n_files.sort()
    for file in n_files:
        s = file.split("/")
        s[3] = "{:01d}".format(int(s[3]))
        s = '/'.join(s)
        r_files.append(s)
    return r_files
    

def flatten_cords(single_mat):
    XX,YY = np.meshgrid(np.arange(single_mat.shape[1]),np.arange(single_mat.shape[0]))
    flattened = np.vstack((single_mat.ravel(),XX.ravel(),YY.ravel())).T
    return flattened

variables = ["sst","sla","sw","u","v","uera","vera"]

BASE = "data/INPUT_AI/"
data_vars = []
for variable in variables:
    paths = sort_over_folders(BASE,variable)
    data = []
    
    for path in paths:
        print(path)
        nc_file = nc.Dataset(path)
        nc_data = ma.array(nc_file.variables[variable][:].data,mask=nc_file.variables[variable][:].mask)
        nc_file.close()
        data.append(nc_data[0])
    
    data = ma.stack(data)    
    data_vars.append(data)


data_vars = ma.stack(data_vars,axis=0)

t_vars = []
for index, variable in enumerate(variables):
    t_serie = data_vars[index,...]
    time_steps, nlat, nlon = t_serie.shape[0], t_serie.shape[1], t_serie.shape[2]
    t_var = np.empty([time_steps, nlat*nlon])
    for i in range(time_steps):
        t_var[i,...] = flatten_cords(t_serie[i,...])[:,0]
        t_var[t_var == -9999.0] = np.NaN
    t_vars.append(t_var)
t_vars = np.stack(t_vars, axis=1)
t_vars = t_vars.reshape([63724,216,7])

lats_flat = flatten_cords(t_serie[i,...])[:,2]
lons_flat = flatten_cords(t_serie[i,...])[:,1]
        
for index, item in enumerate(variables):
    variables[index] = item+"_"

scalers = []
for i in range(63724):
    s = StandardScaler()
    s.fit(t_vars[i,...])
    joblib.dump(s,"data/flat_variables/scalers/"+str(int(lats_flat[i]))+"_"+str(int(lons_flat[i]))+"_scaler.pkl")
    scalers.append(s)

data = pd.read_csv("data/svm_mlp_data.csv")

def mon_sin(x):
    return np.sin(2*np.pi*x/12)

def mon_cos(x):
    return np.cos(2*np.pi*x/12)

data = data[data["hl_"] != -9999.0]
data = data[data["sla_"] != -9999.0]
data = data[data["u_"] != -9999.0]
data = data[data["v_"] != -9999.0]
data = data[data["uera_"] != -9999.0]
data = data[data["vera_"] != -9999.0]
data["lat"] = data["lat"].apply(np.sin)
data["lon_1"] = data["lon"].apply(np.sin)
data["lon_2"] = data["lon"].apply(np.cos)
data["mon_1"] = data["mon"].apply(mon_sin)
data["mon_2"] = data["mon"].apply(mon_cos)


data["chl_"] = data["hl_"]
data = data.drop(columns=["hl_","Unnamed: 0"])

for i in range(63724):
    lat = int(lats_flat[i])
    lon = int(lons_flat[i])
    curr = data.loc[(data["lat"] == lat) & (data["lon"] == lon),variables]
    if curr.shape != (0,7):
        s = scalers[i]
        data.loc[(data["lat"] == lat) & (data["lon"] == lon),variables] = s.transform(curr)

def split_scale_data(data): 
    from sklearn.model_selection import train_test_split
    from sklearn.preprocessing import StandardScaler
    l = list(data.columns)
    l.pop(0)
    Y = np.log(data["hl_"])
    X = data[l]
    s = StandardScaler()
    scaled_X = s.fit_transform(X.values)
    X_train, X_val, y_train, y_val = train_test_split(scaled_X, Y, test_size=0.2, random_state=0)
    return X_train, X_val, y_train, y_val, s

m = mlp_1000_500()
tensorboard = TensorBoard(log_dir="tensorboardlogs/PIX{}".format(time()))
mc = ModelCheckpoint('./models/mlp_1000_500_PIX.h5', monitor='val_loss')
es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=150)
print(m.summary())
m.fit(X_train, y_train,verbose=1,batch_size=256,epochs=2000,validation_data=[X_val, y_val], callbacks=[tensorboard, mc, es] )




